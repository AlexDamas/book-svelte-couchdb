
```bash
npx degit sveltejs/template svelte-app
cd svelte-app
```

*Note that you will need to have [Node.js](https://nodejs.org) installed.*


## Get started

Install the dependencies...

```bash
cd svelte-app
npm install
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

##CouchDB

Install with your current OS 

https://couchdb.apache.org/

Go to project fauxton
```bash
http://localhost:5984
```

> Create your database => name : books, partionning : non partionning => create


##If it isn't already done call in svelte app like this

```bash     
      let pdb = new PouchDB('db');
      const replicate = PouchDB.sync('db', 'http://localhost:5984/books', {
          live: true,
          retry: true
      }) 
```
